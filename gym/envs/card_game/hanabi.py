"""
Hanabi
"""

import gym
import sys
import traceback
import random
from six import StringIO
from gym import spaces
from gym import error
from gym.utils import seeding
from .hanabi_logic.hanabi_game import HanabiGame


class HanabiEnv(gym.Env):
	"""
	Hanabi environment.
	"""
	metadata = {"render.modes": ["ansi", "human"]}

	def __init__(self, num_players, debug_flag, illegal_move_mode):
		"""
		Args:
			num_players: the number of players in this game of Hanabi, should be between 2 and 5 (inclusive)
			illegal_move_mode: What to do when the agent makes an illegal move. Choices: 'raise' or 'lose'
		"""

		# Create an instance of HanabiGame, which will be used to track the state
		# and check the logic and results of each play
		self.hanabi_game = HanabiGame(num_players, debug_flag)

		self.illegal_move_mode = illegal_move_mode

		# One action for each possible choice
		self.action_space = spaces.Discrete(59)

		self.observation_space = spaces.Discrete(34)

		self.num_players = num_players

	def sample_move(self):
		return random.randint(0, 59)

	def _reset(self):
		seed = self.seed()[0]
		self.state = self.hanabi_game.reset(seed)
		self.to_play = 1
		self.done = False
		return self.state

	# State, Reward, Done, Info
	def _step(self, action):

		# If already terminal, then don't do anything
		if self.done:
			return self.state, 0., True, {'state': self.state}

		try:
			self.hanabi_game.make_move(action)
		except Exception as ex:
			#print("Exception: " + str(ex))
			#for frame in traceback.extract_tb(sys.exc_info()[2]):
				#fname, lineno, fn, text = frame
				#print("\tError in %s on line %d" % (fname, lineno))
			if self.illegal_move_mode == 'raise':
				raise
			elif self.illegal_move_mode == 'lose':
				#print("GAME OVER: " + str(ex))
				# Automatic loss on illegal move
				self.done = True
				# Decide on lose rewards
				return self.state, -1, True, {'state': self.state}
			else:
				raise error.Error('Unsupported illegal move action: {}'.format(self.illegal_move_mode))

		self.state = self.hanabi_game.get_state()

		# Determine if the game is finished, and if so what our reward is
		reward = self.hanabi_game.game_finished()
		self.done = (reward != 0)

		if(self.to_play == int(self.num_players)):
			self.to_play = 1
		else:
			self.to_play += 1

		return self.state, reward, self.done, {'state': self.state}

	def _render(self, mode='human', close=False):
		if close:
			return
		board = self.state
		outfile = StringIO() if mode == 'ansi' else sys.stdout
		outfile.write(self.hanabi_game.render(self.to_play))

		if mode != 'human':
			return outfile

	def _seed(self, seed=None):
		self.np_random, seed = seeding.np_random(seed)

		return [seed]