"""
Hanabi
"""

import gym
from gym import spaces
from gym import error
from gym.utils import seeding
from hanabi_game import HanabiGame

"""
Changes to George's hanabi class
 - rename game to HanabiGame, change constructor to accept number of players
 - new method HanabiGame.get_action_space_size(), returns the size of the action space
 - new method HanabiGame.get_observation_space_size(), returns the size of the observation space
 - new method HanabiGame.valid_move(action), returns if the move is valid or not
 - new method HanabiGame.make_move(action), perform the action and return nothing
 - new method HanabiGame.game_finished(), returns the reward received if the game is finished, 0 otherwise
 - new method HanabiGame.render(), prints the current status of the game
"""


class HanabiEnv(gym.Env):
    """
    Hanabi environment. 
    """
    metadata = {"render.modes": ["ansi","human"]}

    def __init__(self, num_players, debug_flag, illegal_move_mode):
        """
        Args:
            num_players: the number of players in this game of Hanabi, should be between 2 and 5 (inclusive)
            illegal_move_mode: What to do when the agent makes an illegal move. Choices: 'raise' or 'lose'
        """
        
        # Create an instance of HanabiGame, which will be used to track the state
        # and check the logic and results of each play
        self.hanabi = new HanabiGame(num_players, debug_flag)                
        
        # One action for each board position and resign
        self.action_space = spaces.Discrete(self.hanabi.get_action_space_size())
        
        observation = self.reset()
        self.observation_space = spaces.Discrete(self.hanabi.get_observation_space_size())

        self._seed()




	# return: observation, reward, done, info
	def _step(self, action):
        # If already terminal, then don't do anything
        if self.done:
            return self.state, 0., True, {'state': self.state}

        if not self.hanabi.valid_move(action):
            if self.illegal_move_mode == 'raise':
                raise
            elif self.illegal_move_mode == 'lose':
                # Automatic loss on illegal move
                self.done = True
                # Decide on lose reward
                return self.state, -1., True, {'state': self.state}
            else:
                raise error.Error('Unsupported illegal move action: {}'.format(self.illegal_move_mode))
        else:
            self.hanabi.make_move(action)
            self.state = hanabi.get_state()

		# Determine if the game is finished, and if so what our reward is
        reward = self.hanabi.game_finished()
        self.done = reward != 0
        
        return self.state, reward, self.done, {'state': self.state}
    
    
    
    
    def _reset(self):
        self.state = self.hanabi.reset()
        self.to_play = 0 # Player 0
        self.done = False

        return self.state



    
    def _render(self, mode='human', close=False):
        if close:
            return
        board = self.state
        outfile = StringIO() if mode == 'ansi' else sys.stdout

		outfile.write(self.hanabi().render())

        if mode != 'human':
            return outfile
        
        

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)

        return [seed]
        
