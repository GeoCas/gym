import tensorflow as tf
import tensorlayer as tl
import gym
import numpy as np
import time
import plotly.plotly as py
import plotly
from plotly.graph_objs import *

# hyperparameters

# grid_size = 9
A_space = 60
InputLayerSize = 69
HiddenLayerSize = InputLayerSize * 3
batch_size = 50
learning_rate = 2e-5
gamma = 0.99
decay_rate = 0.9999
render = False  # display the game environment
np.set_printoptions(threshold=np.nan)

def prepro(I):
    return np.reshape(I, (-1, InputLayerSize))


def generate_action(exploration_threshold, player):
    # Either select a choice probabilistically, based on the neural net,
    # or select an action at random. Selecting an action at random
    # allows for exploration

    prob = player.sess.run(
        player.sampling_prob,
        feed_dict={player.states_batch_pl: x}
    )

    if (np.random.rand() < exploration_threshold):
        choice = np.random.choice(list(range(A_space)), p=prob.flatten())
        player.move_arr[choice] += 1
        return choice
    else:
        choice = np.random.choice(list(range(A_space)))
        player.move_arr[choice] += 1
        return choice

class PlayerNet():
    def __init__(self, id):
        print("Player %d net initialized" %id)
        self.id = id
        self.move_arr = [0]*60
        self.xs, self.ys, self.rs = [], [], []
        # observation for training and inference
        self.states_batch_pl = tf.placeholder(tf.float32, shape=[None, InputLayerSize])
        # policy network
        self.network = tl.layers.InputLayer(self.states_batch_pl, name='input_layer_'+str(id))
        self.network = tl.layers.DenseLayer(self.network, n_units=HiddenLayerSize,
                                       act=tf.nn.relu, name='relu1_'+str(id))
        self.network = tl.layers.DenseLayer(self.network, n_units=HiddenLayerSize,
                                            act=tf.nn.relu, name='relu2_' + str(id))
        self.network = tl.layers.DenseLayer(self.network, n_units=HiddenLayerSize,
                                            act=tf.nn.relu, name='relu3_' + str(id))
        self.network = tl.layers.DenseLayer(self.network, n_units=A_space,
                                       act=tf.identity, name='output_layer_'+str(id))
        probs = self.network.outputs
        self.sampling_prob = tf.nn.softmax(probs)

        self.actions_batch_pl = tf.placeholder(tf.int32, shape=[None])
        self.discount_rewards_batch_pl = tf.placeholder(tf.float32, shape=[None])
        loss = tl.rein.cross_entropy_reward_loss(probs, self.actions_batch_pl,
                                                 self.discount_rewards_batch_pl)
        self.train_op = tf.train.RMSPropOptimizer(learning_rate, decay_rate).minimize(loss)

    def start_session(self):
        self.sess = tf.Session()  # sess.close() when finished with this
        init = tf.global_variables_initializer()
        self.sess.run(init)

env = gym.make('Hanabi-v0')
observation = env.reset()
prev_x = None
running_reward = None
running_turn_count = None
reward_sum = 0
episode_number = 0
exploration_threshold = 0.5
player_net_list = []
num_players = int(env.num_players)

for n in range(0,num_players):
    player_net_list.append(PlayerNet(n))
    player_net_list[n].start_session()

start_time = time.time()
game_number = 0
turn_number = 0

reward_arr = []
batch_running_total_arr = []
batch_running_total = 0

while True:
    player = player_net_list[turn_number%num_players]
    if render:
        env.render()
    # elif episode_number % 10000 == 0:
    #     env.render()
    try:
        x = prepro(observation)
    except Exception as ex:
        raise ex

    action = generate_action(exploration_threshold, player)
    observation, reward, done, _ = env.step(action)
    reward_sum += reward
    batch_running_total+= reward

    player.xs.append(x)  # all observations in a episode
    player.ys.append(action)  # all fake labels in a episode (action begins from 1, so minus 1)
    player.rs.append(reward)  # all rewards in a episode
    turn_number += 1

    if done:

        reward_arr.append(reward)
        episode_number += 1
        game_number = 0

        running_turn_count = turn_number if running_turn_count is None else running_turn_count * 0.99 + turn_number * 0.01
        turn_number = 0

        if episode_number % batch_size == 0:
            if episode_number % (batch_size * 25) == 0:
                print(
                    'Completed episode %d, batch completed: \n Running mean: %f\n Running mean turn count: %f' % (
                    episode_number, running_reward, running_turn_count))

                batch_running_total_arr.append(batch_running_total/1250.0)
                batch_running_total = 0

                if (episode_number>10000):

                    plotly.offline.plot({
                        "data": [Scatter(y=reward_arr)],
                        "layout": Layout(title="Reward per episode")
                    })

                    py.plot({
                        "data": [Scatter(y=batch_running_total_arr)],
                        "layout": Layout(title="Mean reward data per batch")
                    })

                    py.plot({
                        "data":[Bar(y=player_net_list[0].move_arr)],
                        "layout": Layout(title="Distribution of action choices for player 1")
                    })

                    py.plot({
                        "data": [Bar(y=player_net_list[1].move_arr)],
                        "layout": Layout(title="Distribution of action choices for player 2")
                    })

                    py.plot({
                        "data": [Bar(y=player_net_list[2].move_arr)],
                        "layout": Layout(title="Distribution of action choices for player 3")
                    })

            for player in player_net_list:
                try:
                    epx = np.vstack(player.xs)
                    epy = np.asarray(player.ys)
                    epr = np.asarray(player.rs)
                    disR = tl.rein.discount_episode_rewards(epr, gamma)
                    disR -= np.mean(disR)
                    stdDev = np.std(disR)
                    if(stdDev!=0):
                        disR /= stdDev

                    player.xs, player.ys, player.rs = [], [], []

                    player.sess.run(
                        player.train_op,
                        feed_dict={
                            player.states_batch_pl: epx,
                            player.actions_batch_pl: epy,
                            player.discount_rewards_batch_pl: disR
                        }
                    )
                except Exception as ex:
                    print("Exception: " + str(ex))

        running_reward = reward_sum if running_reward is None else running_reward * 0.99 + reward_sum * 0.01
        # print('resetting env. episode reward total was %f. running mean: %f' % (reward_sum, running_reward))
        reward_sum = 0
        observation = env.reset()  # reset env
        prev_x = None

    if reward != 0:
        # print(('episode %d: game %d took %.5fs, reward: %f' %
        #            (episode_number, game_number,
        #            time.time()-start_time, reward)),
        #            ('' if reward == -1 else ' !!!!!!!!'))
        start_time = time.time()
        game_number += 1