from card import Card

class Debug:
	'Stores debug info'

	def __init__(self, game):
		self.hand_memory = []

		for x in range (0,5):
			self.hand_memory.append([-1,-1,-1,-1,-1])

	def add_card_to_memory(self, player_num, card_hand_position, suit, value):
		player_hand_array = self.hand_memory[player_num - 1]
		mem_card = player_hand_array[card_hand_position - 1]

		if (mem_card == -1):
			player_hand_array[card_hand_position - 1] = Card(suit, value)
		else:
			if (mem_card.suit == None):
				mem_card.suit = suit
			elif (mem_card.value == -1):
				mem_card.value = value


	def discard_card_memory(self, player_id, cardNum):
		self.hand_memory[player_id - 1][cardNum - 1] = -1

	def display_hand_memory(self):
		for hand in self.hand_memory:
			for card in hand:
				if(card == -1):
					print("Unknown card")
				else:
					print(str(card))
			print()