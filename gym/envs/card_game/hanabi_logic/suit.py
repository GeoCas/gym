from enum import Enum
class Suit(Enum):
	Red = 0
	Blue = 1
	Green = 2
	White = 3
	Yellow = 4