from deck import Deck

#Basic demonstration of deck functionality

deck1 = Deck()
deck1.print_stock()
deck1.shuffle()
print("------------------------ SHUFFLE ------------------------")
deck1.print_stock()
for x in range (0,60):
	card1 = deck1.draw_card()
	if(card1!=0):
		print("------------------------ DRAW CARD ------------------------")
		print(card1.value + " " + card1.suit)
		print("Remaining stock " + str(deck1.stock_count()))