from card import Card
from suit import Suit

my_card1 = Card(Suit.Red,1)
my_card2 = Card(Suit.Red,2)
my_card3 = Card(Suit.Red,3)
my_card4 = Card(Suit.Red,4)
my_card5 = Card(Suit.Red,5)
my_card6 = Card(Suit.Blue,1)
my_card7 = Card(Suit.Blue,2)
my_card8 = Card(Suit.Blue,3)

print(str(hash(my_card1)))
print(str(hash(my_card2)))
print(str(hash(my_card3)))
print(str(hash(my_card4)))
print(str(hash(my_card5)))
print(str(hash(my_card6)))
print(str(hash(my_card7)))
print(str(hash(my_card8)))