from player import Player
from card import Card

my_player = Player()
my_card1 = Card(2, "Red")
my_card2 = Card(1, "Blue")
my_card3 = Card(4, "Green")
my_card4 = Card(6, "Pink")

print("Give cards")
my_player.give_card(my_card1)
my_player.give_card(my_card2)
my_player.give_card(my_card3)
my_player.give_card(my_card4)
my_player.display_hand()

for x in range (0,4):
	card_remove = input("Which card would you like to discard? ")

	print("\nCard discarded")
	my_player.discard_card(int(card_remove))

	my_player.display_hand()