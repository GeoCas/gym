class Card:
	'Stores card value and suit'

	def __init__(self, suit, value):
		self.suit = suit
		self.value = int(value)

	def __eq__(self, other):
		try:
			return self.suit == other.suit and self.value == other.value

		except AttributeError:
			return False;

	def __hash__(self):

		if(self.suit.name=="Red"):
			return self.value-1
		elif(self.suit.name=="Blue"):
			return 5 + self.value-1
		elif (self.suit.name == "Green"):
			return 10 + self.value-1
		elif (self.suit.name == "White"):
			return 15 + self.value-1
		elif (self.suit.name == "Yellow"):
			return 20 + self.value-1

		return -1

	def __str__(self):
		try:
			return (self.suit.name + " " + str(self.value))
		except AttributeError:
			return ("Unknown suit, " + str(self.value))