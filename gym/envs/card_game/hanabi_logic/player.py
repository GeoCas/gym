from illegal_move_exception import IllegalMoveException

class Player:
	'Defines player traits'

	def __init__(self, id, game):
		self.id = id
		self.game = game
		self.hand = []
		self.latest_card_index = None
		self.name = str(id) #Set name to id provisionally, then replace using set_name()

	def give_card(self, new_card):

		for ndx, member in enumerate(self.hand):
			if(member is None):
				self.hand[ndx] = new_card
				self.latest_card_index=ndx
				return


		self.hand.append(new_card)

	def get_card(self,cardNum):
		try:
			return self.hand[cardNum-1]
		except Exception as ex:
			raise IllegalMoveException("Cannot play/discard a card that does not exist in hand")

	def discard_card(self,cardNum):
		try:
			self.hand[cardNum-1] = None
		except Exception as ex:
			raise IllegalMoveException("Cannot discard a card that does not exist in hand")

	def draw_card(self, deck):
		if (len(deck.stock) != 0):
			self.give_card(deck.draw_card())
			info = "New card " + str(self.latest_card_index+1) + " drawn"
		else:
			info = "No more cards to be drawn: Final Round!"
			if (self.game.final_round):
				return info
			else:
				self.game.final_round = True
				self.game.turns_remaining = self.game.num_players + 1

		return info

	def str_hand(self):

		hand = ""

		counter=1
		for x in range(0,len(self.hand)):
			if(self.latest_card_index==x):
				hand += "Card " + str(counter) + ":- " + str(self.hand[x]) + " (Newest card)\n"
			else:
				hand += "Card " + str(counter) + ":- " + str(self.hand[x]) + "\n"
			counter+=1

		return hand

	def set_name(self):
		self.name = input("What is your name? ")

	def __str__(self):
		return str(self.id)