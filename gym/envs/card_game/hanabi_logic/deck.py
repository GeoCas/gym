import itertools
import random
from card import Card
from suit import Suit

class Deck:
	'A deck contains 50 cards, 10 for each suit. Each suit has three 1s, two 2s, two 3s, two 4s and one 5.'

	def __init__(self):
		self.stock = []
		self._loadDeck()

	def _loadDeck(self):
		suits = Suit.Red, Suit.Green, Suit.Blue, Suit.Yellow, Suit.White
		values = '1112233445'

		for card in itertools.product(suits, values):
			self.stock.append(Card(card[0], card[1]))

	def reset(self, seed):
		self.stock = []
		self._loadDeck()
		random.seed(seed)
		random.shuffle(self.stock)

	def print_stock(self):
		for card in self.stock:
			print(card)

	#The lowest index in stock is considered the top card of the deck face down
	def draw_card(self):
		if(len(self.stock)>0):
			return self.stock.pop()
		else:
			print("No cards left to draw")
			return 0