from player import Player
from validator import *

class Human(Player):
	'Sublcass of Player to define human behavior'

	def __init__(self, id):
		Player.__init__(self, id)
		self.ai = False

	#Gets input where upper is the highest valid input and invalid_list is an array containing invalid values.
	def get_move(self, upper, invalid_list):
		input = int_in_range_input(1, upper, invalid_list)
		return input