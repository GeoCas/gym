from deck import Deck
from state import State
from player import Player
from debug import Debug
from human import Human
from invalid_value import  Invalid_Value
from action_converter import ActionConverter
from illegal_move_exception import IllegalMoveException

class HanabiGame:
	'Defines game logic and manages turn based play'

	def __init__(self, num_players, debug_flag):
		self.num_players = int(num_players)
		self.debug_flag = debug_flag

		# Debug
		self.debug = Debug(self)

		# Deck
		self.deck = Deck()

		# Move converter
		self.action_converter = ActionConverter(self)

	def reset(self, seed):
		# Deck
		self.deck.reset(seed)

		# Tokens
		self.clue_count = 8
		self.fuse_count = 0

		# Suits
		self.played_cards = [[], [], [], [], []]

		# Data
		self.turn_number = 0
		self.score = 0
		self.discard_pile = []
		self.turns_remaining = self.num_players
		self.final_round = False
		self.action_info = ""

		# Players
		self.player_list = []

		# Hand sizes
		if (self.num_players > 3):
			self.game_hand_size = 4
		else:
			self.game_hand_size = 5

		# Hand dealing
		for x in range(0, self.num_players):
			new_player = Player(x + 1, self)
			for n in range(0, self.game_hand_size):
				new_player.give_card(self.deck.draw_card())
			self.player_list.append(new_player)

		self.current_player = self.player_list[0]

		# Environment
		self.state = State(self)
		self.done = False

		return self.state.environment

	###### RENDER ######

	def render(self, to_play):
		self.current_player = self.player_list[to_play-1]

		# Print results from step
		render = ""

		if(self.action_info!= ""):
			render += self.action_info

		render += "\n-------------------------------------\n"

		# Display current player number
		render += "Player: " + str(self.current_player.id) + "'s turn\n"

		# Display cards remaining in deck
		render += "Cards remaining in deck: " + str(len(self.deck.stock)) + "\n"

		# Display fuse and clue token count
		render += "Clues remaining: " + str(self.clue_count) + "/8\nFuses lit: " + str(self.fuse_count) + "/4" + "\n"

		# Display played cards
		render += self.str_cards_in_play()

		# Display other player's hands
		render += self.str_player_hands(self.current_player)

		# Display debug
		if(self.debug_flag):
			self.debug.display_hand_memory()

		return render

	def str_player_hands(self, hide_player):
		player_hands = ""

		for player in self.player_list:
			if (player.id != hide_player.id):
				player_hands += "\n" + player.name + "'s hand\n"
				player_hands += player.str_hand()

		return player_hands

	def get_other_player_hands(self):
		player_hands = []

		for player in self.player_list:
			if (player.id != self.current_player.id):
				for card in player.hand:
					if(card is not None):
						player_hands.append(hash(card))
					else:
						player_hands.append(-1)
					empty = 5 - len(player.hand)

				for x in range(0, empty):
					player_hands.append(-1)

			else:
				for x in range (0,5):
					player_hands.append(-1)

		while(len(player_hands) != 25):
			player_hands.append(-1)
		return player_hands

	def str_cards_in_play(self):
		cards_in_play = "Cards in play: "

		for played_suit in self.played_cards:
			for card in played_suit:
				cards_in_play += str(card) + " "

		cards_in_play += "\n"

		return cards_in_play

	###### STEP ######

	def make_move(self, action):
		self.turn_number+=1
		self.action_converter.convert_action(action)
		return True

	def get_state(self):
		return self.state.generate_state()

	def game_finished(self):
		if (self.fuse_count == 4):
			reward = self.score**2
			# print("\n====GAME OVER====")
			# print("4/4 fuses lit")
			# print("Final score: " + str(self.score))
			# print("Reward: " + str(reward))
			return reward

		if(len(self.deck.stock) == 0):
			self.final_round == True

		if (self.turns_remaining == 0):
			reward = self.score**2
			# print("\n====GAME OVER====")
			# print("No cards remaining to draw")
			# print("Final score: " + str(self.score))
			# print("Reward: " + str(reward))
			return reward

		if(self.score==25):
			print("\n====VICTORY====")
			return self.score**2

		return 0

	## ACTIONS ##

	def action_give_info(self, info_player, info_card_num, info_type):
		# If suit=True then information is for suit. If false information is for value

		if(self.clue_count==0):
			raise IllegalMoveException("Illegal move, no clue tokens left to use")

		if (self.current_player.id == info_player.id):
			raise IllegalMoveException("Illegal move, cannot give information to self")

		info_card = info_player.get_card(info_card_num)

		if(info_type=='Suit'):
			self.action_info = ("\nMove made to give suit information: \n")
			for x in range(0, len(info_player.hand)):
				current_card = info_player.hand[x]
				if (current_card.suit == info_card.suit):
					self.action_info += "Player " + info_player.name + "'s ""card " + str(x) + " has a suit of " + str(current_card.suit.name) + "\n"
					self.debug.add_card_to_memory(info_player.id, x + 1, current_card.suit, -1)

		elif(info_type=='Value'):
			self.action_info = ("\nMove made to give value information: \n")
			for x in range(0, len(info_player.hand)):
				current_card = info_player.hand[x]
				if(current_card is not None):
					if (current_card.value == info_card.value):
						self.action_info += "Player " + info_player.name + "'s ""card " + str(x) + " has a value of " + str(current_card.value) + "\n"
						self.debug.add_card_to_memory(info_player.id, x, None, current_card.value)

		self.clue_count -= 1

		self.final_round_check()

	def action_discard_card(self, card_num):
		discard_card = self.current_player.get_card(card_num)

		if(discard_card is None):
			raise IllegalMoveException("Illegal move, cannot discard a card that does not exist")

		if(self.clue_count == 8):
			raise IllegalMoveException("Illegal move, cannot discard a card when all clue tokens are available")

		self.action_info = "Discarded card " + str(card_num) + " (" + str(self.current_player.get_card(card_num)) + ")"
		self.current_player.discard_card(card_num)
		self.current_player.draw_card(self.deck)

		if(self.debug_flag):
			self.debug.discard_card_memory(self.current_player.id, card_num)

		self.clue_count+=1

		self.final_round_check()

	def action_play_card(self, card_num):
		play_card = self.current_player.get_card(card_num)

		if (play_card is None):
			raise IllegalMoveException("Illegal move, cannot play a card that does not exist")

		self.action_info = "Attempted to play: " + str(play_card) + "\n"

		if(self._legal_play(play_card)):
			suit_pile = self.played_cards[play_card.suit.value]
			suit_pile.append(play_card)
			self.score += 1
			self.current_player.discard_card(card_num)
			self.current_player.draw_card(self.deck)

			self.action_info+="Play successful"

		else:
			self.action_info = "Cannot play " + str(play_card) + ", fuse token added"
			self.fuse_count+=1
			self.current_player.discard_card(card_num)
			self.current_player.draw_card(self.deck)

		self.final_round_check()

	def final_round_check(self):
		if(self.final_round):
			self.turns_remaining -= 1

	def _legal_play(self, play_card):
		suit_pile = self.played_cards[play_card.suit.value]

		if (int(play_card.value) == 1 and len(suit_pile) == 0):  # If card is 1 and no cards in suit pile
			return True

		legal_play = False

		for card in suit_pile:
			if (card == play_card):  # If play card matches one already in pile
				return False
			elif (int(card.value) == int(play_card.value) - 1):  # Otherwise, if the value of the card is 1 less than the played card, set flag to true
				legal_play = True

		return legal_play