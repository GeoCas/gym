from deck import Deck
from state import State
from debug import Debug
from human import Human
from invalid_value import  Invalid_Value

class HanabiGame:
	'Defines game logic and manages turn based play'

	def __init__(self, num_players, debug_flag):
		self.num_players = num_players
		self.debug_flag = debug_flag
		self.reset()

	def reset(self):
		# Deck
		self.deck = Deck()

		# Players
		self.player_list = []
		self.game_hand_size = 0

		# Tokens
		self.clue_count = 8
		self.fuse_count = 0

		# Actions
		self.actions = []

		# Suits
		self.played_cards = [[], [], [], [], []]

		# Data
		self.debug = Debug(self)
		self.round_number = 0
		self.score = 0
		self.discard_pile = []

		self.turns_remaining = 0
		self.final_round = False

		# Environment
		self.state = State(self)
		self.done = False

		return self.state

	def valid_move(self, action):
		return move_evaluator.try_move(action, self)


	def display_player_hands(self, hide_player):
		"""Prints hands of all players other than hide_player
			Args:
				hide_player (Player): Player to be hidden from display
		"""

		for players in self.player_list:
			if(players!=hide_player):
				print("\n" + players.name + "'s hand")
				players.display_hand()

	def display_cards_in_play(self):
		"""Prints all cards that have been successfully played
		"""

		print("\nCards in play:")

		for played_suit in self.played_cards:
			for card in played_suit:
				print(str(card))
		print()

	def display_player_list(self, hide_player):
		"""Prints numbered list of all players other than hide_player.
			Args:
				hide_player (Player): Used to hide a Player from the listing (Set to None to show all players)
		"""

		counter = 1
		for player in self.player_list:
			if (player.name != hide_player.name):
				print(str(counter) + ". " + player.name)
			else:
				print(str(counter) + ". " + player.name + " (cannot give information to self)")
			counter += 1

	def display_game_info(self, current_player):
		""""Prints game info for a given round. Includes Player names, cards remaining, clue tokens remaining and fuse
		tokens remaining
			Args:
				current_player (Player) : The Player whose turn it is (used to print their name)
		"""

		print("\n-------------------------------------")
		print("Player: " + current_player.name + "'s turn\n")
		print("Cards remaining in deck: " + str(len(self.deck.stock)))
		print("Clue tokens: " + str(self.clue_count))
		print("Fuse tokens: " + str(self.fuse_count) + "/4")
		self.display_player_hands(current_player)
		self.display_cards_in_play()

	def display_available_actions(self):
		"""Prints player actions (Give info, discard card, play card) and whether they are legal choices.
		actions is a global array cleared each turn to store which actions are legal for the current turn. For example,
		[True,False] means info can be given but discards are unavailable (cards can always be played or the game would
		be over)."""

		self.actions.clear()
		if(self.clue_count>0):
			print("1. Give information on a card")
			self.actions.append(True)
		else:
			print("1. Cannot give information - no clue tokens available")
			self.actions.append(False)

		if(self.clue_count<8):
			print("2. Discard a card")
			self.actions.append(True)
		else:
			print("2. Cannot discard a card - all clue tokens available")
			self.actions.append(False)

		print("3. Play a card")

	def action_human_discard_card(self, player):
		"""Handles discarding cards
		:param player(Player): The player discarding the card
		:return:
		"""

		print("Card number to discard: ")
		card_num = player.get_move(len(player.hand), [])
		print(str("Card " + str(card_num) + " (" + str(player.get_card(card_num))) + ") discarded")
		discard = self.current_player.get_card(card_num)
		self.discard_pile.append(discard)
		player.discard_card(card_num)
		player.give_card(self.deck.draw_card())
		self.clue_count+=1

		self.debug.discard_card_memory(player.id, card_num)

	def action_play_card(self, card_num):

		try:
			card = self.current_player.get_card(card_num)
		except IndexError:
			print("Index out of range error: Could not get card from player's hand - out of range (e.g accessing card 5 when player only has 4 cards - ABORT)")

		suit = self.played_cards[card.suit.value]

		if (self.legal_play(card)):
			suit.append(card)
			self.score += 1

			self.current_player.discard_card(card_num)
			print("Played: " + str(card))
			self.draw_card(self.current_player)

		else:
			print("Cannot play: " + str(card))
			print("Fuse token added")
			self.fuse_count += 1

			discard = self.current_player.get_card(card_num)
			self.discard_pile.append(discard)
			self.current_player.discard_card(card_num)
			self.draw_card(self.current_player)

		self.debug.discard_card_memory(self.current_player.id, card_num)

	def action_human_play_card(self, player):
		"""Handles playing cards
		:param player(Player): The player playing a card
		:return:
		"""

		print("Card number to play: ")
		card_num = player.get_move(len(player.hand), [])
		card = player.get_card(card_num)
		suit = self.played_cards[card.suit.value]

		if(self.legal_play(card)):
			suit.append(card)
			self.score += 1

			player.discard_card(card_num)
			print("Played: " + str(card))
			self.draw_card(player)

		else:
			print("Cannot play: " + str(card))
			print("Fuse token added")
			self.fuse_count+=1

			player.discard_card(card_num)
			self.draw_card(player)

		self.debug.discard_card_memory(player.id, card_num)

	def legal_play(self, played_card):
		"""Given the current state of the game, checks to see if a card can be legally played. If so it is added to
		played_cards, otherwise a fuse token is accrued.
		:param played_card(Card): The card attempted to be played
		:return:
		"""
		#if card is 1, is its list empty? if not then illegal move
		#if card is greater than 1, is its preceeding card there? if not illegal move
		#if card is already there then illegal move

		suit_pile = self.played_cards[played_card.suit.value]

		if(int(played_card.value)==1):
			if len(suit_pile)!=0:
				return False
			else:
				return True

		preceding_card = False

		for card in suit_pile:
			if(card==played_card):
				return False
			elif(int(card.value)==int(played_card.value)-1):
				preceding_card = True

		return preceding_card

	def draw_card(self, player):
		"""Handles drawing of cards
		:param player(Player): Player to receive the newly drawn card
		:return:
		"""
		if (len(self.deck.stock) != 0):
			player.give_card(self.deck.draw_card())
			print("New card " + str(player.latest_card_index+1) + " drawn")
		else:
			print("No more cards to be drawn: Final Round!")
			if (self.final_round):
				return
			else:
				self.final_round = True
				self.turns_remaining = len(self.players_list) + 1

	def get_invalid_actions(self):
		"""Helper function to construct the array containing Invalid_Value objects (value + error messages) based on the
		 global actions array.
		:return([]): Constructed list of
		"""
		invalid_list = []

		if (self.actions[0] == False):
			invalid_list.append(Invalid_Value(1, "Cannot give information, no clue tokens remaining"))

		if (self.actions[1] == False):
			invalid_list.append(Invalid_Value(2, "Cannot discard card when all clue tokens are in play"))

		return invalid_list

	def is_game_over(self):
		"""Checks game over conditions
			Returns:
				bool: True if game is over (win or loss), False otherwise
		"""

		if(self.fuse_count==4):
			print("\n====GAME OVER====")
			print("4/4 fuses lit")
			return True

		if(self.final_round and self.turns_remaining==0):
			print("\n====GAME OVER====")
			print("No cards remaining to draw")
			return True

		return False

	def is_victory(self):
		"""Checks victory conditions. Note that victory is not the opposite of is_game_over(...) as victory is only
		achieved if all suits are completed.
			Returns:
				bool: True if the game has ended in a victory (all suits completed to 5)
		"""
		for suit in self.played_cards:
			if (len(suit) != 5):
				return False

		print("\n====VICTORY====")
		return True

	def end_game(self):
		"""End the game
		:return:
		"""
		print("Final score " + str(self.score))
		self.state.update_state()
		exit()

	def run_game(self, player_count):
		"""Main game loop
		:param player_count: The number of players in the game
		:return:
		"""
		print("Starting game for " + str(player_count) + " players")
		self.deck.shuffle()

		if(player_count>3):
			self.game_hand_size = 4
		else:
			self.game_hand_size=5

		##For each player get name and deal hand
		for x in range (0,player_count):
			if(x==0):
				new_player = AI(x+1)
			else:
				new_player = AI(x+1)
			#new_player.set_name()
			for n in range(0, self.game_hand_size):
				new_player.give_card(self.deck.draw_card())
			self.player_list.append(new_player)

		##Main game loop
		while True:
			self.round_number+=1
			for player in self.player_list:
				self.current_player = player

				# Human player
				if(player.ai == False):
					self.display_game_info(player)
					if(self.debug_flag):
						print("Debug:")
						self.debug.display_hand_memory()

					self.display_available_actions()
					choice=player.get_move(3,self.get_invalid_actions())
					self.perform_action(player, choice)

					if(self.final_round):
						self.turns_remaining-=1

					if(self.is_game_over() or self.is_victory()):
						self.end_game()

					print("\nNext player's turn, press any key")
					input()

				# AI player
				else:
					print("\n-------------------------------------")
					print("Player: " + self.current_player.name + "'s turn\n")
					player.get_move(self);

				self.state.update_state()
