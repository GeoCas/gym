from player import Player
from random import randint
from validator import *
import time

class ActionConverter(Player):
   'Sublcass of Player to define AI behavior'

   def __init__(self, game):
      self.game = game

   def convert_action(self, action):

      #If in range 0 to 24 - Attempt to give SUIT information to player 1,2,3,4.5 regarding card 1,2,3,4,5
      if(action<=24):
         if(action<=4):
            self.game.action_give_info(self.game.player_list[0], action % 5 + 1, 'Suit')
         elif(action<=9):
            self.game.action_give_info(self.game.player_list[1], action % 5 + 1, 'Suit')
         elif (action <= 14):
            self.game.action_give_info(self.game.player_list[2], action % 5 + 1, 'Suit')
         elif (action <= 19):
            self.game.action_give_info(self.game.player_list[3], action % 5 + 1, 'Suit')
         elif (action <= 24):
            self.game.action_give_info(self.game.player_list[4], action % 5 + 1, 'Suit')

      #If in range 24 to 49 - Attempt to give VALUE information to player 1,2,3,4 regarding card 1,2,3,4,5
      elif (action <= 49):
            if (action <= 29):
               self.game.action_give_info(self.game.player_list[0], action % 5 + 1, 'Value')
            elif (action <= 34):
               self.game.action_give_info(self.game.player_list[1], action % 5 + 1, 'Value')
            elif (action <= 39):
               self.game.action_give_info(self.game.player_list[2], action % 5 + 1, 'Value')
            elif (action <= 44):
               self.game.action_give_info(self.game.player_list[3], action % 5 + 1, 'Value')
            elif (action <= 49):
               self.game.action_give_info(self.game.player_list[4], action % 5 + 1, 'Value')

      #If in range 49 to 54 - attempt to discard card 1,2,3,4,5
      elif (action<=54):
         self.game.action_discard_card(action % 5 + 1)

      #If in range 54 to 59 - attempt to play card 1,2,3,4,5
      elif (action<=59):
         self.game.action_play_card(action % 5 + 1)

