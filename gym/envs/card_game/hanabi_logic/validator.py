
#Checks if int value is in range of low and upper bounds(inclusive)
def int_in_range(value, lower, upper, invalid_list):

	error_message = "Invalid input: \"" + str(value) + "\"" + ". Must be number in range " + str(lower) + " to " + str(upper) + ", please try again"

	try:

		for invalid in invalid_list:
			if(invalid.value==int(value)):
				print(invalid.error_message)
				return False

		if (int(value)<lower or int(value)>upper):
			print(error_message)
			return False

	except ValueError:
		print(error_message)
		return False

	return True

def invalid_input(value, message):
	print("Invalid input: \"" + str(value) + "\", " + message)

def int_in_range_input(lower, upper, invalid_list):
	while(True):
		user_input = input()
		if(int_in_range(user_input,lower,upper, invalid_list)):
			return int(user_input)