class State:
	'Defines environment variables for AI'

	def __init__(self, hanabi_game):

		self.game = hanabi_game
		self.cards_remaining = []

		self.environment = []
		self.update_state()

	def update_state(self):
		current_player_num = self.game.current_player.id  # Current player
		fuse_count = self.game.fuse_count  # Fuses lit
		clue_count = self.game.clue_count # Clues remaining
		cards_left = len(self.game.deck.stock)  # Number of cards left in deck
		cards_played = self.get_cards_played()  # Length 5 array with number representing highest card played in suit
		self.get_cards_remaining()  # Number of each unique card remaining in play (either in hands or in deck)
		self.hand_memory = self.convert_hand_memory()  # Length 10 array of current player's memory of their own hand. Each card represented by 2 int pair for value/suit. -1 for unknown
		self.other_hands = self.game.get_other_player_hands() # Length 20 array (4 players x 5 cards) to represent other player's hands visible to the current player

		self.environment = [current_player_num, fuse_count, clue_count, cards_left] + cards_played + self.cards_remaining + self.hand_memory + self.other_hands

	def get_cards_remaining(self):
		self.cards_remaining = [0] * 25

		for card in self.game.deck.stock:
			i = hash(card)
			self.cards_remaining[i] += 1

		for player in self.game.player_list:
			for card in player.hand:
				if(card!=None):
					i = hash(card)
					self.cards_remaining[i] += 1

	def get_cards_played(self):
		cards_played = []
		for suit in self.game.played_cards:
			cards_played.append(len(suit))
		return cards_played

	def generate_state(self):
		self.update_state()
		return self.environment

	def convert_hand_memory(self):
		#Converts n-sized array of cards to 2n sized array of suit/value where -1 represents unknown for either attribute
		old_hand_memory = self.game.debug.hand_memory[self.game.current_player.id -1]

		new_hand_memory = []

		for card in old_hand_memory:
			try:
				suit = card.suit.value

				if (suit == None):
					new_hand_memory.append(-1)
				else:
					new_hand_memory.append(suit)

			except AttributeError:
				new_hand_memory.append(-1)

			try:
				value = card.value

				if (value == -1):
					new_hand_memory.append(-1)
				else:
					new_hand_memory.append(value)

			except AttributeError:
				new_hand_memory.append(-1)

		return new_hand_memory

	def __str__(self):
		return "State information: " + str(self.environment)