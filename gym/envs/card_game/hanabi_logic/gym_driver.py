import gym
env = gym.make('Hanabi-v0')
for i_episode in range(1):
	observation = env.reset()
	for t in range(10000):
		print(observation)
		env.render()

		while True:	
			try:
				action = env.sample_move()
				print("\n------> Action number: " + str(action))
				observation, reward, done, info = env.step(action)
				break
			except Exception as ex:
				pass

		if done:
			print("Reward was: " + str(reward))
			print("Episode finished after {} timesteps".format(t+1))
			break
