"""
HiLo
"""

import gym
import sys
from gym import spaces
from gym import error
from gym.utils import seeding
from hilo_game import HiloGame

class HiloEnv(gym.Env):
	"""
	Hilo environment.
	"""
	metadata = {"render.modes": ["ansi","human"]}

	def __init__(self, illegal_move_mode):
		"""
		Args:
			illegal_move_mode: What to do when the agent makes an illegal move. Choices: 'raise' or 'lose'
		"""

		# Create an instance of HanabiGame, which will be used to track the state
		# and check the logic and results of each play
		self.hilo = HiloGame()

		#Illegal move mode
		self.illegal_move_mode = illegal_move_mode

		# Two actions, 0 for lower, 1 for higher
		self.action_space = spaces.Discrete(2)

		observation = self.reset()
		self.observation_space = spaces.Discrete(1)

		self._seed()

	# return: observation, reward, done, info
	def _step(self, action):
		#If already terminal, then don't do anything
		if self.done:
			return self.state, 0., True, {'state': self.state}

		if not self.hilo.valid_move(action):
			if self.illegal_move_mode == 'raise':
				raise
			elif self.illegal_move_mode == 'lose':
				# Automatic loss on illegal move
				self.done = True
				# Decide on lose reward
				return self.state, -1., True, {'state': self.state}
			else:
				raise error.Error('Unsupported illegal move action: {}'.format(self.illegal_move_mode))
		else:
			self.hilo.make_move(action)
			self.state = self.hilo.get_state()

		# Determine if the game is finished, and if so what our reward is
		reward = self.hilo.game_finished()
		self.done = (reward == 0)

		return self.state, reward, self.done, {'state': self.state}




	def _reset(self):
		self.state = self.hilo.reset()
		self.done = False

		return self.state

	def _render(self, mode='human', close=False):
		if close:
			return
		outfile = StringIO() if mode == 'ansi' else sys.stdout
		outfile.write("Current number: " + str(self.hilo.number) + "\n")

		if mode != 'human':
			return outfile


	def _seed(self, seed=None):
		self.np_random, seed = seeding.np_random(seed)

		return [seed]
        
