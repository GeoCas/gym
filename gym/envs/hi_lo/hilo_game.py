from random import randint
	#Generate number 1-10
	# Display number
	# Ask if higher x = [true/false]
	# Evaluate y = number[0] < number[1]
	# If x!=y
	# Done=yes

class HiloGame():
	def __init__(self):
		self.number = 0
		self.move = 0
		self.finished = 0


	def reset(self):
		self.number = self.random_num()
		self.finished = 0
		return self.number

	def random_num(self):
		return (randint(1, 10))

	def valid_move(self, action):
		if(0<=action<=1):
			return True
		return False

	def make_move(self, action):
		if(action):
			print("AI says HIGHER")
		else:
			print("AI says LOWER or equal")

		new_number = self.random_num()
		if((new_number > self.number) == action):
			self.finished = 0
		else:
			self.finished = 1

		self.number = new_number

	def get_state(self):
		return self.number

	def game_finished(self):
		if(self.finished):
			print("Failed, number was: " + str(self.number) + "\n")
			return 0
		else:
			return 1